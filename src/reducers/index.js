// Root reducers (Chứa reducers chính)
import { combineReducers } from "redux";

import taskReducer from "./task.reducer";

const rootReducers = combineReducers({
    taskReducer
});

export default rootReducers;